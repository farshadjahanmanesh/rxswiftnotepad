//
//  HomeViewController.swift
//  RxSwiftNotePad
//
//  Created by HamiDev0 on 11/3/1397 AP.
//  Copyright © 1397 AP farshadJahanmanesh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

 
/// home view
class HomeViewController: UIViewController {
    //MARK:------------------------- Outlets ---------------------------
    
    var noteArray = [
        HomeUIModel.Folders(Category: "ICLOUD", Title: "All iCloud", Count: 13),
        HomeUIModel.Folders(Category: "ICLOUD", Title: "Notes", Count: 8),
        HomeUIModel.Folders(Category: "ICLOUD", Title: "New Folder", Count: 2),
        HomeUIModel.Folders(Category: "ICLOUD", Title: "New Folder 1", Count: 2),
        HomeUIModel.Folders(Category: "ICLOUD", Title: "New Folder 2", Count: 1),
        HomeUIModel.Folders(Category: "ICLOUD", Title: "New Folder 3", Count: 0)
    ]
    
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var tblHome: UITableView!
    //MARK:------------------------- Properties ---------------------------
    
    //MARK:------------------------- View Controller Functions ---------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        let objArray : Observable<[HomeUIModel.Folders]> = Observable.just(noteArray)
        
        objArray.asObservable().subscribe(onNext: {
            updatedArray in
            print(updatedArray)
        }).disposed(by: disposeBag)
        
        objArray.bind(to: tblHome.rx.items(cellIdentifier: "cell")){ _ , Folder, cell in
            if let cellToUse = cell as? HomeTableViewCell{
                cellToUse.lable1.text = Person.name
                cellToUse.lable2.text = ("\(Person.age)")
            }
            }.disposed(by: disposeBag)
        
        tblHome.rx.modelSelected(HomeUIModel.Folders.self).subscribe(onNext: { Folder in
            print(Folder.Title)
        }).disposed(by: disposeBag)
        
        
    }
    //MARK:------------------------- My Functions ---------------------------

}

//
//  HomeUIModel.swift
//  RxSwiftNotePad
//
//  Created by HamiDev0 on 11/3/1397 AP.
//  Copyright © 1397 AP farshadJahanmanesh. All rights reserved.
//

struct HomeUIModel  : Codable {
    
    struct Folders {
        let Category : String
        let Title : String
        let Count : Int
    }
    
}

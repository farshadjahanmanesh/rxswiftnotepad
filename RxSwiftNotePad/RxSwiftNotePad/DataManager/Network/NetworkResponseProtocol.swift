//
//  NetworkResponseProtocol.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/17/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation

/// Every response has to implements this protocol, this is what the Network give back to the callers
protocol NetworkResponseProtocol {
    /// cached
    var cached : Bool {get}
    
    /// the request that cause this response
    var request : NetworkRequestProtocol {get}
    
    ///raw response, can be anything
    var response : Data? {get}
    
    /// if request has any error
    var error : Error?{get}
    
    /// response code
    var httpCode : Int{get}
}


// MARK: - all network response extensions would be here like toJson,toObject...
extension NetworkResponseProtocol {
    
    /// convert received data to a Dictionary
    ///
    /// - Returns: Dictionary of received data
    
    func toJSON() -> [String:Any]? {
        //get the response data
        guard let data = self.response else { return nil}
        
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Codable]
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
        
    }
    func toObject<T>() -> T? where T : Decodable {
        //get the response data
        guard let data = self.response else { return nil}
        
        do {
            let decoder = JSONDecoder()
            
            return try decoder.decode(T.self, from: data)
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
        
    }
}

//
//  NetworkProtocol.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/15/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
enum NetworkHttpMethod : String {
    case OPTIONS = "OPTIONS"
    case GET     = "GET"
    case HEAD    = "HEAD"
    case POST    = "POST"
    case PUT     = "PUT"
    case PATCH   = "PATCH"
    case DELETE  = "DELETE"
    case TRACE   = "TRACE"
    case CONNECT = "CONNECT"
}

enum NetworkRequestParseMethod {
    case json,formFields,strinf,base64
}

struct NetworkConfig {
    
    var retry : Int
    
    /// our base url, this can retrieve from config file or filled manually,
    /// like https://hamiSystem.ir
    var baseUrl : String
    
    /// response time out
    var responseTimeOut : Int
    
    /// connect time out
    var connectTimeOut : Int
    
    ///request time out
    var requestTimeOut : Int
    
    /// indicates if this request has to pin our ssl
    var sslPinning : Bool
    
    /// default headers attached to all requests
    var defaulHeaders : [String:String]
    
    /// init our config file with default data
    init() {
        retry = 0
        baseUrl = ""
        responseTimeOut = 0
        connectTimeOut = 0
        requestTimeOut = 0
        sslPinning = false
        defaulHeaders = ["Content-Type":"json"]
    }
    
    /// if any request wants to change default network config, it can get defaults from here and made its changes and pass this config to the request
    static var defaultConfig : NetworkConfig {
        get {
            return NetworkConfig()
        }
    }
}

protocol NetworkProtocol {
    /// do a get request
    ///
    /// - Parameters:
    ///   - request: network request object
    ///   - callBack: this function would call after request completed(either success or error)
    func get(request : NetworkRequestProtocol,callBack : ((_:NetworkResponseProtocol)->Void)?)
    
    
    /// post would post data to the server and retrieve its result
    ///
    /// - Parameters:
    ///   - request: request object with postType
    ///   - callBack: this function will call after request executed completely
    func post(request : NetworkRequestProtocol,callBack : ((_:NetworkResponseProtocol)->Void)?)
    
    
    /// custom is where we explicy indicates that our request is GET,POST,PUT,DELETE...
    ///
    /// - Parameters:
    ///   - request: request object with postType
    ///   - callBack: this function will call after request executed completely
    func custom(type : NetworkHttpMethod,request : NetworkRequestProtocol,callBack : ((_:NetworkResponseProtocol)->Void)?)
    
    /// network need a config file to set its default variables
    var configs : NetworkConfig{get}
    
}

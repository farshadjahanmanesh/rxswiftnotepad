//
//  NetworkResponseProtocol.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/17/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation

class NetworkResponse : NetworkResponseProtocol {
    var cached : Bool
    var httpCode: Int
    
    var request: NetworkRequestProtocol
    
    var response: Data?
    
    var error: Error?
    
    init(request : NetworkRequestProtocol) {
        self.request = request
        self.httpCode = 0
        self.cached = false
    }
}


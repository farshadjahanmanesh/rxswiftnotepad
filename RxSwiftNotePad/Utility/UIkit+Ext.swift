//
//  Extensions.swift
//  RxSwiftNotePad
//
//  Created by ios 4 on 10/21/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import UIKit
extension UIViewController {
    /// find viewcontroller with similar storyboardid as viewcontroller name in MainStoryboard
    ///
    /// - Returns: ViewController
    static func fromMainStoryboard<T>()->T? where T : UIViewController{
        let myName = String(describing: T.self)
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: myName) as? T
    }
    
    /// find storyboard with the same name as ViewController and instanciate ViewController with same storyboardid
    ///
    /// - Returns: ViewController
    static func fromStoryboardWithSameName<T>()->T? where T : UIViewController{
        let myName = String(describing: T.self)
        return UIStoryboard(name: myName, bundle: nil).instantiateViewController(withIdentifier: myName) as? T
    }
    
    /// find storyboard with the same name as ViewController and instanciate initial ViewController
    ///
    /// - Returns: ViewController
    static func intialViewControllerFromStoryboardWithSameName<T>()->T? where T : UIViewController{
        let myName = String(describing: T.self)

        return UIStoryboard(name: myName, bundle: nil).instantiateInitialViewController() as? T
    }
}

//
//  CacheProtocol.swift
//  RxSwiftNotePad
//
//  Created by ios 4 on 10/21/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
enum CachValidationEnum {
    case valid,expired
}
protocol CacheProtocol {
    /// this indicates value expiration(Second), if any item value cached more than this, its value is expired and needs to refresh
    var defaultCacheExpirationTime: Double{set get}
    
    /// cache a simple string
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheString (URL : String , Value : String)
    
    /// cache data
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheData (URL : String , Value : Data)
    
    /// cache objects of any, note that you have to convert this to whatever you want after fetching
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheAny (URL : String , Value : Any)
    
    /// cache a simple string
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheString (cacheItem : CacheItem , Value : String)
    
    /// cache data
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheData (cacheItem : CacheItem , Value : Data)
    
    /// cache objects of any, note that you have to convert this to whatever you want after fetching
    ///
    /// - Parameters:
    ///   - URL: unique key for saving and retrieving data, it will overrided if this key is exists
    ///   - Value: value to be saved
    func cacheAny (cacheItem : CacheItem , Value : Any)
    /// retrieve a simple string by its key
    ///
    /// - Parameter URL: key of this value
    /// - Returns: string value
    func fetchString (URL : String) -> String?
    
    /// retrieve data by its key
    ///
    /// - Parameter URL: key of this value
    /// - Returns: Data value
    func fetchData (URL : String) -> Data?
    
    /// retrieve value by its key
    ///
    /// - Parameter URL: key of this value
    /// - Returns: any value, convert it to whatever you want
    func fetchAny (URL : String) -> Any?
    
    
    /// delete a cache by its id
    ///
    /// - Parameter URL: key to be deleted
    func deleteCache (URL : String)
    
    /// delete multiple cache item by those keys
    ///
    /// - Parameter URL: keys to be deleted
    func deleteCache (URL : String...)
    
}

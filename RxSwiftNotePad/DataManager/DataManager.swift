//
//  DataManager.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/15/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation

//Types defined and uses in DataManager
public struct CacheItem : Codable{
    var cacheKey: String
    var expirationTime : Double?
    var cancelers : [String]?
}

final class DataManager {
    //handles the networking
    var networking : NetworkProtocol
    
    //handle cache
    var caching : CacheProtocol
    //handle database
    //var db :
    
    //handle temporary cache and data management like memory
    //var limbo :
    init(network : NetworkProtocol
        ,cache : CacheProtocol
        //,limbo :
        //,database :
        ) {
        self.networking = network
        self.caching = cache
    }
    
}

//Apis
extension DataManager {
    
    /// check cache before network, this is usefull when we want something cached and also sometimes use this cache for sake of preformance, for caching, the request cache or cacheTheResult properties should be setted.
    ///
    /// - Parameters:
    ///   - request: request object
    ///   - callBack: cached or fresh data
    func checkCacheThenGetNetwork(request : NetworkRequestProtocol,callBack : ((NetworkResponseProtocol)->Void )?){
        //check cache
        if let data = caching.fetchData(URL: request.cache?.cacheKey ?? request.fullUrlWithQueryString) {
            let network = NetworkResponse.init(request: request)
            network.cached = true
            network.response = data
            callBack?(network)
        }else
        //check network
        {
            self.networking.get(request: request){response in
                if response.error == nil, let responseData = response.response{
                    if let cache = request.cache {
                        //clean previous value and relative caches
                        self.removeRelativesCaches(cache)
                        
                        //save new value
                        self.caching.cacheData(cacheItem : cache, Value: responseData)
                    }else if request.cacheTheResult {
                        self.caching.cacheData(URL:request.fullUrlWithQueryString, Value: responseData)
                    }
                }
                callBack?(response)
            }
        }
    }
    
    func removeRelativesCaches(_ cacheItem : CacheItem){
        guard let relativesCachesKeys = cacheItem.cancelers else {
            return
        }
        for cache in relativesCachesKeys {
            self.caching.deleteCache(URL: cache)
        }
    }
}


//
//  MyAlamofire.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/15/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
import Alamofire
class MyAlamofire : NetworkProtocol {
    func custom(type: NetworkHttpMethod, request: NetworkRequestProtocol, callBack: ((NetworkResponseProtocol) -> Void)?) {
        let networkResponse = NetworkResponse(request: request)
        guard let url = URL(string: request.fullPath) else {
            networkResponse.error = URLError(.badURL)
            callBack?(networkResponse)
            return
        }
        
        //set default config if request not set it
        if request.networkConfig == nil {
           request.setConfig(config: self.configs)
        }
        
        Alamofire.request(url, method: HTTPMethod.init(rawValue: request.method.rawValue) ?? .get, parameters: request.stringQuery, encoding: JSONEncoding.default, headers: request.headers)
            .responseJSON { response in
                switch response.result {
                case .success(_) :
                    networkResponse.response = response.data
                case .failure(let error) :
                    networkResponse.error = error
                }
                
                callBack?(networkResponse)
        }
    }
    
    var configs: NetworkConfig
    init(config : NetworkConfig) {
        self.configs = config
    }
    
   
    func get(request: NetworkRequestProtocol, callBack: ((NetworkResponseProtocol) -> Void)?) {
        
        let networkResponse = NetworkResponse(request: request)
        guard let url = URL(string: request.fullPath) else {
            networkResponse.error = URLError(.badURL)
            callBack?(networkResponse)
            return
        }
        if request.networkConfig == nil {
            request.setConfig(config: self.configs)
        }
        Alamofire.request(url, method: .post, parameters: request.stringQuery, encoding: JSONEncoding.default, headers: request.headers)
            .responseJSON { response in
                switch response.result {
                case .success(_) :
                    networkResponse.response = response.data
                case .failure(let error) :
                    networkResponse.error = error
                }
                
                callBack?(networkResponse)
        }
    }
    
    func post(request: NetworkRequestProtocol, callBack: ((NetworkResponseProtocol) -> Void)?) {
        
        let networkResponse = NetworkResponse(request: request)
        guard let url = URL(string: request.fullUrlWithQueryString) else {
            networkResponse.error = URLError(.badURL)
            callBack?(networkResponse)
            return
        }
        if request.networkConfig == nil {
            request.setConfig(config: self.configs)
        }
        Alamofire.request(url, method: .post, parameters: request.body, encoding: JSONEncoding.default, headers: request.headers)
            .responseJSON { response in
                switch response.result {
                case .success(_) :
                    networkResponse.response = response.data
                case .failure(let error) :
                    networkResponse.error = error
                }
                
                callBack?(networkResponse)
        }
    }
    
}

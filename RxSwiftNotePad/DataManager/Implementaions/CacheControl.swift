//
//  CacheControl.swift
//  CacheControl
//
//  Created by Hamidreza Jamshidi on 7/24/1397 AP.
//  Copyright © 1397 AP Hamidreza Jamshidi. All rights reserved.
//


import UIKit

final internal class Cacher : CacheProtocol {
    func cacheString(cacheItem: CacheItem, Value: String) {
        self.archive(key: cacheItem.cacheKey, value: Value, expirationAfterMinute: cacheItem.expirationTime)
    }
    
    func cacheData(cacheItem: CacheItem, Value: Data) {
          self.archive(key: cacheItem.cacheKey, value: Value, expirationAfterMinute: cacheItem.expirationTime)
    }
    
    func cacheAny(cacheItem: CacheItem, Value: Any) {
          self.archive(key: cacheItem.cacheKey, value: Value, expirationAfterMinute: cacheItem.expirationTime)
    }
    
    public var defaultCacheExpirationTime: Double = 24 //(24 minute)
    
    public func deleteCache(URL: String) {
        UserDefaults.standard.removeObject(forKey: URL.generateKeys.mainKey)
        UserDefaults.standard.removeObject(forKey: URL.generateKeys.dateKey)
        UserDefaults.standard.synchronize()
    }
    
    public func deleteCache(URL: String...) {
        URL.forEach { (str) in
            deleteCache(URL: str)
        }
    }
    
    public func cacheString(URL: String, Value: String) {
        self.archive(key: URL, value: Value)
    }
    
    public func cacheData(URL: String, Value: Data) {
        self.archive(key: URL, value: Value)
    }
    
    public func cacheAny(URL: String, Value: Any) {
        self.archive(key: URL, value: Value)
    }
    
    public func fetchString(URL: String) -> String? {
        //check if cache is valid else delete it
        if isCacheValueExpired(URL) == .expired {
            self.deleteCache(URL: URL)
            return nil
        }
        
        //retreive cache
        guard let fetchValue = self.unarchive(key: URL) as? String else {
            return nil
        }
        return fetchValue
    }
    
    public func fetchData(URL: String) -> Data? {
        //check if cache is valid else delete it
        if isCacheValueExpired(URL) == .expired {
            self.deleteCache(URL: URL)
            return nil
        }
        
        //retreive cache
        guard let fetchValue = self.unarchive(key: URL) as? Data else {
            return nil
        }
        return fetchValue
    }
    
    public func fetchAny(URL: String) -> Any? {
        //check if cache is valid else delete it
        if isCacheValueExpired(URL) == .expired {
            self.deleteCache(URL: URL)
            return nil
        }
        
        //retreive cache
        guard let fetchValue = self.unarchive(key: URL) else {
            return nil
        }
        return fetchValue
    }
    
    
    /// archive object and save it to the cache, we save two object each time, first, we save an object by provieded key and value, and the we save second one by key+"data" and value of Date(), we use the second one eachtime a fetch request arrives to validate value (expired or not)
    ///
    /// - Parameters:
    ///   - key: Unique if, if exists, overrided
    ///   - value: value of this key
    ///   - date: this date will use for checking expiration of the cache, this is the time when this cache is saved
    private func archive(key : String, value: Any?, expirationAfterMinute: Double? = nil) {
        
        guard let data = value , let encodedData: Data = try? NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false) else {
            return
        }
        UserDefaults.standard.set(encodedData, forKey: key.generateKeys.mainKey)
        //convert minute to seconds
        let expiredAt = getCachedDate(date: Date().addingTimeInterval(60 * (expirationAfterMinute ?? defaultCacheExpirationTime)))
        UserDefaults.standard.set(expiredAt, forKey: key.generateKeys.dateKey)
        UserDefaults.standard.synchronize()
    }
    
    
    /// fetch data from disk find by its key
    ///
    /// - Parameter key: item key
    /// - Returns: retreived data
    private func unarchive(key: String) -> Any? {
        
        guard  let decodedData = UserDefaults.standard.object(forKey: key.generateKeys.mainKey) as? Data ,  let decoded = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedData) else {
            return nil
        }
        return decoded
        
    }
    
    /// convert date to our well formated
    ///
    /// - Parameter date: date
    /// - Returns: well formated date
    private func getCachedDate (date : Date?) -> Double {
        let cacheDate : Double = (date ?? Date()).timeIntervalSince1970
        return cacheDate
    }
    
    //check if this cache is expired, if it is expired we remove it else we return its value
    private func isCacheValueExpired(_ key: String) -> CachValidationEnum {
        let makeUniqueIdWithDate = key.generateKeys.dateKey
        //get expirationTimeStamp value (TimeStamp)
        guard let expiredAt = UserDefaults.standard.object(forKey: makeUniqueIdWithDate) as? Double else {
            return .expired
        }
        
        //get now
        let now = Date().timeIntervalSince1970
        
        //check if this value is less than our default cache expiration time, if it is less that that, this value is valid, else this is expired
        if now < expiredAt {
            //cache is valid
            return .valid
        } else {
            //cache is expired
            return  .expired
        }
    }
}

extension String {
    // we using this property in archive and unarchive (when we save and retreive) and deleting
    fileprivate var generateKeys : (mainKey:String,dateKey:String){
        get {return (self,"\(self)/date")}
    }
}

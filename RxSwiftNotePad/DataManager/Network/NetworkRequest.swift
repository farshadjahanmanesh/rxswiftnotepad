//
//  NetworkResponseProtocol.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/17/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
class NetworkRequest : NetworkRequestProtocol {
    // setting this will automatic active or deactive cacheTheResult property
    var cache: CacheItem? {
        didSet {
            self.cacheTheResult = cache != nil
        }
    }
    
    //this property would be setted either user set cache or directly set this option, if this property active
    var cacheTheResult: Bool
    
    var fullUrlWithQueryString: String {
        get {
            return baseUrl + path + (self.stringQuery != nil ? "?" + self.stringQuery!.map({return "\($0)=\($1)"}).joined(separator: "&") : "")
        }
    }
    
    var bodyParseMode: NetworkRequestParseMethod
    
    var method: NetworkHttpMethod
    
    var networkConfig: NetworkConfig?
    
    
    /// setting the config, here we ignore already existing header keys and just append new keys in our request headers, it means that if we add some extera headers, when we set new config, config headers appended to our headers and existing keys will be ignored and keep their old value
    ///
    /// - Parameter config: configs
    func setConfig(config: NetworkConfig) {
        self.networkConfig = config
        
        //if we have headers
        if let headers = self.headers  {
            // get existing keys
            let existingKeys = Set(headers.keys)
            
            //get new keys and find keep only new ones
            var configKeys = Set(config.defaulHeaders.keys)
                configKeys.subtract(existingKeys)
            
            //append new keys to the headers
            for key in configKeys {
                self.add(extraHeaders: [key:(config.defaulHeaders[key] ?? "") ])
            }
        }
    }
    
    var path: String
    var baseUrl : String {
        get {
            return networkConfig?.baseUrl ?? ""
        }
    }
    var fullPath: String {
        return (networkConfig?.baseUrl ?? "") + path
    }
    
    lazy var headers: [String:String]? = {
         return [String:String]()
    }()
    
    var stringQuery: [String:Any]?
    
    var body: [String:Any]?
    
    init() {
        path = ""
        //headers = NetworkConfig.defaultConfig.defaulHeaders
        //networkConfig = NetworkConfig.defaultConfig
        method = .GET
        bodyParseMode = .json
        cacheTheResult = false
    }
    @discardableResult func add(parameter param: [String:Any]) -> Self {
        self.stringQuery = param
        return self
    }
    
    @discardableResult func add(extraHeaders param: [String:String]) -> Self {
        param.forEach { (key,value) in
            self.headers?.updateValue(value, forKey: key)
        }
    
        return self
    }
    
    @discardableResult func add(path: String) -> Self {
        self.path = path
        return self
    }
//
//    @discardableResult func add(base: String) -> Self {
//        self.networkConfig?.baseUrl = base
//        return self
//    }
    
    @discardableResult func add(body: [String:Any]) -> Self {
        self.body = body
        return self
    }
    
    @discardableResult func defaultRequest() -> Self {
        return self
    }
    @discardableResult func set(cache : CacheItem) -> Self {
        self.cache = cache
        return self
    }
    @discardableResult func cacheResult() -> Self {
        self.cacheTheResult = true
        return self
    }
}

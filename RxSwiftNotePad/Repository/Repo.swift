//
//  File.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/15/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
import RxSwift

//response type just for sake of simplifing
typealias ResponseTuple<T> = (value : T?,error : Error?)

protocol  RepositoryProtocol {
    func retrieveUsersWith(params : [String : Codable]) -> Observable<ResponseTuple<[Models.Users]>>
}
class Repository : RepositoryProtocol {

    // keep list of cached items, expiration and those services which this service whould cancel
    private(set) var cacheItems : [CacheItem]
    
    /// the is our data manager
    private let _dataManager : DataManager
    
    
    /// initialize our repository with our datamanager
    ///
    /// - Parameter dataManager: Data Manager Object
    init(dataManager :  DataManager) {
        _dataManager = dataManager
        cacheItems = []
        if let cacheItemsJsonPath = Bundle.main.path(forResource: "CacheItems", ofType: "json"), let json = try? String(contentsOfFile: cacheItemsJsonPath) , let jsonData = json.data(using: String.Encoding.utf8){
            if let items = try? JSONDecoder().decode([CacheItem].self, from: jsonData) {
                cacheItems = items
            }
        }
        
        
    }
    
    /// retrieve users from datamanager
    ///
    /// - Parameter param: parameters to pass to the query
    /// - Returns: an Observable of the response and the error(if exists)
    func retrieveUsersWith(params : [String : Codable]) -> Observable<ResponseTuple<[Models.Users]>>{
        
        //create an empty observable and call the request and wait for that request to resolve
        let observable = Observable<ResponseTuple<[Models.Users]>>.create{
            observer in
            
            //create the request for object
            let request : NetworkRequestProtocol = NetworkRequest()
                .add(path: "/5bcc2b87310000700028c4f3")
                .add(parameter: ["lastUpdate": "1396/3/32"])
                .add(extraHeaders: ["device":"iOS"])
                .cacheResult()
            
            //this example is about when we do not want the default config and we want to config this request by our selves
            var config = NetworkConfig()
            config.sslPinning = false
            config.baseUrl = "http://www.mocky.io/v2"
            request.setConfig(config: config)
            
            //call the network and wait for the result
            
            self._dataManager.checkCacheThenGetNetwork(request: request) {(result) in
                
                //if there is an error, tell subsribers
                if let error = result.error {
                    //observer.onNext((nil, error ))
                    observer.onError(error)
                }
                //else go and pass the result
                else {
                    let list : [Models.Users] = result.toObject() ?? []
                    observer.onNext((list, nil))
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
        
        //return the observable object for observers
        return  observable
        
    }
}










//
//  Models.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/15/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
class Models {
    struct Users : Codable{
        var name : String
        var family : String
        var image : String
        var protileUrl : String
        var id  : Int
    }
}

//
//  Bootstrap.swift
//  RxSwiftNotePad
//
//  Created by farshad jahanmanesh on 10/16/18.
//  Copyright © 2018 HamiSystem. All rights reserved.
//

import Foundation
import UIKit
final class BootStrap {
    private let window : UIWindow?
    init(window : UIWindow?) {
        self.window = window
    }
    func boot(){
        //create our modules
        let network = MyAlamofire(config: NetworkConfig.defaultConfig)
        let cache = Cacher()
        //create our data manager
        let datamaneger = DataManager(network: network,cache : cache)
        
        //initialize repository
        let repository = Repository(dataManager: datamaneger)
        
//        guard let homeViewController  : HomeViewController = HomeViewController.fromStoryboardWithSameName() else {
//            fatalError()
//        }
//        //init our view controller dependencies
//        let homeUseCase = HomeUseCase(repository: repository)
//        homeViewController.viewModel = HomeViewModel(homeCase: homeUseCase)
//        self.window?.rootViewController = homeViewController
    }
}
